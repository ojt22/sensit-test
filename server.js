const Net = require("net")

const WebSocketServer = {
  port: 80,
  socket: null,
  init: function () {
    const server = new Net.Server();

    server.listen(this.port, () => {
      console.log(
        `Server listening for connection requests on socket localhost:${this.port}`
      );
    });

    server.on("connection", (socket) => {
      console.log("A new connection has been established.");
      this.socket = socket;
      this.socket.setTimeout(600000);
      socket.on("data", (data) => {
        logResponse(data, "general listener");
      });

      socket.on("end", () => {
        console.log("Closing connection with the client");
        this.socket = null;
      });

      socket.on("error", (err) => {
        console.log(`Error: ${err}`);
      });
    });
  },

  sendPacket: function (hex, bytes_length, type) {
    return new Promise((res, rej) => {   
      if (!this.socket) {
        rej(new Error("no connection with car server"));
      }
      this.socket.on("data", (data) => {
        if (data) {
          logResponse(data, type);
          res(readableString);
        } else {
          rej(new Error("server rejected request"));
        }
      });
      byte_array = toByteArrayWithLogs(hex, type);
      IMEI = "866770052058873:";
      end = ":+++"
      bufferIMEI = Buffer.from(IMEI, 'utf8')
      bufferLength = Buffer.from(bytes_length + ":", 'utf8')
      bufferCmd = Buffer.from(hex, 'hex')
      bufferEnd = Buffer.from(end, 'utf8')
      concatedBuffer = Buffer.concat([bufferIMEI, bufferLength, bufferCmd, bufferEnd])
      console.log("send: " + type + " Byte Array: ", concatedBuffer);
      this.socket.write(concatedBuffer);
    });
  },

  sendText: function () {
    return new Promise((res, rej) => {   
      if (!this.socket) {
        rej(new Error("no connection with car server"));
      }
      this.socket.on("data", (data) => {
        if (data) {
          res(readableString);
        } else {
          rej(new Error("server rejected request"));
        }
      });
      hex = '680B0B6873FD5226049046FFFFFFFFBE16'
      byte_array = toByteArrayWithLogs(hex, "test");
      val = "866770052058873:17:";
      val2 = ":+++"
      buffer = Buffer.from(val, 'utf8')
      buffer2 = Buffer.from(hex, 'hex')
      buffer3 = Buffer.from(val2, 'utf8')
      concatedBuffer = Buffer.concat([buffer, buffer2, buffer3])
      console.log("send: " + packetType + " Byte Array: ", concatedBuffer);
      this.socket.write(concatedBuffer);
    });
  },

  testConvert: function(hex, packetType) {
    toByteArrayWithLogs(hex, packetType);
    logResponse(new Uint8Array([16, 64, 254, 62, 22]), packetType)
    return new Promise((res, rej) => {   
      res("see logs in server");
    });
  },
};



function toByteArrayWithLogs(hex, packetType) {
  console.log("")
  console.log("Request")
  console.log("")
  console.log("send: " + packetType + " Hex: " + hex);

  // Convert the formatted hexadecimal string to a byte array
  hexArray = hex.match(/.{1,2}/g); // Split the hex string into pairs of characters

  byte_array = new Uint8Array(hexArray.map(byte => parseInt(byte, 16)));
  console.log("send: " + packetType + " Byte array: ", byte_array);
  responseBuffer = Buffer.from(byte_array);
  readableString = responseBuffer.toString("hex");
  console.log("send: " + packetType + "sent Byte array to Hex: ", readableString);


  return byte_array;
}

function logResponse(response, packetType) {
  console.log("")
  console.log("Response")
  console.log("")
  console.log("get: " + packetType + " response: " + response);

  responseBuffer = Buffer.from(response);
  readableString = responseBuffer.toString("hex");
  console.log("get: " + packetType + " Byte array to Hex: ", readableString);

  readableString = responseBuffer.toString("utf-8");
  console.log("get: " + packetType + " Byte array to utf-8: ", readableString);

}

module.exports = WebSocketServer;
