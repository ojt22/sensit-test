        const getResponsePacket = 'E5';//hexa //16,64,254,62,22 decimal

        const formattedHex = getResponsePacket.replace(/\s/g, "");

            // Convert the formatted hexadecimal string to a byte array
        const byte_array = new Uint8Array(formattedHex.length / 2);
                for (let i = 0; i < formattedHex.length; i += 2) {
            const byte = formattedHex.substr(i, 2);
            byte_array[i / 2] = parseInt(byte, 16);
            }

            console.log('send getResponse: ' + Buffer.from(byte_array));
            const responseBuffer = Buffer.from(byte_array);
        const readableString = responseBuffer.toString("hex");
        console.log("once data", readableString);
    