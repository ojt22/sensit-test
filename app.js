const express = require("express");
const app = express();
const http = require("http").Server(app);
const socketService = require("./server");


const selectPacket = '680B0B6873FD5226049046FFFFFFFFBE16';
const getDataPacket = '107B007B16';
const offPacket = '6807076807076873FD5101FD1A00D916';
const onPacket = '6807076873FD5101FD1A01DA16';

// const selectPacket = '680B0B6873FD5226049046FFFFFFFFBE16';//46900426 unit number
// const getResponsePacket = '10 40 FE 3E 16';//hexa //16,64,254,62,22 decimal
// const onPacket = '68 07 07 68 73 FD 51 01 FD 1A 01 DA 16';
// const offPacket = '68 07 07 68 73 FD 51 01 FD 1A 00 D9 16';
// const getDataPacket = '10 7B 00 7B 16';

const port = 3000;
socketService.init();

http.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});

const selectMiddleware = (req, res, next) => {
    socketService.sendPacket(selectPacket, 17, "select Packet")
      .then(serRes => {
        console.log("select serRes: ", serRes);
        next();
      })
      .catch(e => {
        res.status(500).send(e.message);
      });
};

app.use(selectMiddleware);

app.get("/testConvert", (req, res) => {
  console.log("original hex: 1040FE3E16  , expected Byte_array: 16,64,254,62,22")
  socketService.testConvert(getResponsePacket, "testConvert").then(serRes => {
    console.log("hex serRes: ",serRes);
    res.send(serRes);
  }).catch(e => {
    res.status(500).send(e.message);
  });
});
app.get("/sendText", (req, res) => {
  socketService.sendText().then(serRes => {
    console.log("hex serRes: ",serRes);
    res.send(serRes);
  }).catch(e => {
    res.status(500).send(e.message);
  });
});

app.get("/hex", (req, res) => {
  socketService.sendPacket(hex, "custom hex Packet").then(serRes => {
    console.log("hex serRes: ",serRes);
    res.send(serRes);
  }).catch(e => {
    res.status(500).send(e.message);
  });
});


app.get("/select", (req, res) => {
  socketService.sendPacket(selectPacket, 17, "select Packet").then(serRes => {
    console.log("select serRes: ",serRes);
    res.send(serRes);
  }).catch(e => {
    res.status(500).send(e.message);
  });
});

app.get("/on", (req, res) => {
 console.log("change")
  socketService.sendPacket(onPacket, 13, "on Packet").then(serRes => {
    console.log("on serRes: ",serRes);

    res.send(serRes);
  }).catch(e => {
    res.status(500).send(e.message);
  });
});

app.get("/off", (req, res) => {
  socketService.sendPacket(offPacket, 16, "off Packet").then(serRes => {
    console.log("off serRes: ",serRes);
    res.send(serRes);
  }).catch(e => {
    res.status(500).send(e.message);
  });
});

app.get("/getData", (req, res) => {
  socketService.sendPacket(getDataPacket, 5, "get Data Packet").then(serRes => {
    console.log("getData serRes: ",serRes);
    res.send(serRes);
  }).catch(e => {
    res.status(500).send(e.message);
  });
});


// app.get("/getResponse", (req, res) => {
//   socketService.sendPacket(getResponsePacket, "get Response Packet").then(serRes => {
//     console.log("getResponse serRes: ",serRes);
//     res.send(serRes);
//   }).catch(e => {
//     res.status(500).send(e.message);
//   });
// });

// app.get("/onWithSelect", (req, res) => {
//   socketService.sendPacket(onPacketWithSelect, "onPacketWithSelect").then(serRes => {
//     console.log("hex serRes: ",serRes);
//     res.send(serRes);
//   }).catch(e => {
//     res.status(500).send(e.message);
//   });
// });

// app.get("/offWithSelect", (req, res) => {
//   socketService.sendPacket(offPacketWithSelect, "offPacketWithSelect").then(serRes => {
//     console.log("hex serRes: ",serRes);
//     res.send(serRes);
//   }).catch(e => {
//     res.status(500).send(e.message);
//   });
// });

// app.get("/getDataWithSelect", (req, res) => {
//   socketService.sendPacket(getDataPacketWithSelect, "getDataPacketWithSelect").then(serRes => {
//     console.log("hex serRes: ",serRes);
//     res.send(serRes);
//   }).catch(e => {
//     res.status(500).send(e.message);
//   });
// });


