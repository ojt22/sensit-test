const Net = require("net")

const WebSocketServer = {
  port: 80,
  socket: null,
  init: function () {
    const server = new Net.Server();

    server.listen(this.port, () => {
      console.log(
        `Server listening for connection requests on socket localhost:${this.port}`
      );
    });

    server.on("connection", (socket) => {
      console.log("A new connection has been established.");
      this.socket = socket;
      socket.on("data", (data) => {
        console.log(data);
      });

      socket.on("end", () => {
        console.log("Closing connection with the client");
        this.socket = null;
      });

      socket.on("error", (err) => {
        console.log(`Error: ${err}`);
      });
    });
  },
};

module.exports = WebSocketServer;